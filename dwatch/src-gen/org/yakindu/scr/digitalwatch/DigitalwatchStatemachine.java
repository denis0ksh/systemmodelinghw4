package org.yakindu.scr.digitalwatch;
import org.yakindu.scr.ITimer;

public class DigitalwatchStatemachine implements IDigitalwatchStatemachine {

	protected class SCIButtonsImpl implements SCIButtons {
	
		private boolean topLeftPressed;
		
		public void raiseTopLeftPressed() {
			topLeftPressed = true;
		}
		
		private boolean topLeftReleased;
		
		public void raiseTopLeftReleased() {
			topLeftReleased = true;
		}
		
		private boolean topRightPressed;
		
		public void raiseTopRightPressed() {
			topRightPressed = true;
		}
		
		private boolean topRightReleased;
		
		public void raiseTopRightReleased() {
			topRightReleased = true;
		}
		
		private boolean bottomLeftPressed;
		
		public void raiseBottomLeftPressed() {
			bottomLeftPressed = true;
		}
		
		private boolean bottomLeftReleased;
		
		public void raiseBottomLeftReleased() {
			bottomLeftReleased = true;
		}
		
		private boolean bottomRightPressed;
		
		public void raiseBottomRightPressed() {
			bottomRightPressed = true;
		}
		
		private boolean bottomRightReleased;
		
		public void raiseBottomRightReleased() {
			bottomRightReleased = true;
		}
		
		protected void clearEvents() {
			topLeftPressed = false;
			topLeftReleased = false;
			topRightPressed = false;
			topRightReleased = false;
			bottomLeftPressed = false;
			bottomLeftReleased = false;
			bottomRightPressed = false;
			bottomRightReleased = false;
		}
	}
	
	protected SCIButtonsImpl sCIButtons;
	
	protected class SCITimeEventsImpl implements SCITimeEvents {
	
		private boolean startTimeEditing;
		
		public void raiseStartTimeEditing() {
			startTimeEditing = true;
		}
		
		private boolean stopTimeEditing;
		
		public void raiseStopTimeEditing() {
			stopTimeEditing = true;
		}
		
		protected void clearEvents() {
			startTimeEditing = false;
			stopTimeEditing = false;
		}
	}
	
	protected SCITimeEventsImpl sCITimeEvents;
	
	protected class SCIDisplayImpl implements SCIDisplay {
	
		private SCIDisplayOperationCallback operationCallback;
		
		public void setSCIDisplayOperationCallback(
				SCIDisplayOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}
	}
	
	protected SCIDisplayImpl sCIDisplay;
	
	protected class SCILogicUnitImpl implements SCILogicUnit {
	
		private SCILogicUnitOperationCallback operationCallback;
		
		public void setSCILogicUnitOperationCallback(
				SCILogicUnitOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}
		private boolean startAlarm;
		
		public void raiseStartAlarm() {
			startAlarm = true;
		}
		
		protected void clearEvents() {
			startAlarm = false;
		}
	}
	
	protected SCILogicUnitImpl sCILogicUnit;
	
	private boolean initialized = false;
	
	public enum State {
		modeSwitching_ChronoMode,
		modeSwitching_TimeMode,
		modeSwitching_TimeEditingMode,
		modeSwitching_RightBottomButtonPressed,
		modeSwitching_LeftBottomButtonPressed,
		modeSwitching_EditingMode,
		modeSwitching_EditingMode_r1_editCurrent,
		modeSwitching_EditingMode_r1_LeftBottomButtonPressed,
		modeSwitching_EditingMode_r1_BottomRightPressed,
		modeSwitching_EditingMode_r2_SelectionOn,
		modeSwitching_EditingMode_r2_SelectionOff,
		light_lightoff,
		light_lighton,
		time_digitalwatch,
		time_timePause,
		chronoOnOff_ChronoOff,
		chronoOnOff_ChronoOn,
		alarm_AlarmOff,
		alarm_AlarmOn,
		alarm_AlarmOn_r1_BlinkOn,
		alarm_AlarmOn_r1_BlinkOff,
		$NullState$
	};
	
	private final State[] stateVector = new State[6];
	
	private int nextStateIndex;
	
	private ITimer timer;
	
	private final boolean[] timeEvents = new boolean[14];
	public DigitalwatchStatemachine() {
		sCIButtons = new SCIButtonsImpl();
		sCITimeEvents = new SCITimeEventsImpl();
		sCIDisplay = new SCIDisplayImpl();
		sCILogicUnit = new SCILogicUnitImpl();
	}
	
	public void init() {
		this.initialized = true;
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		for (int i = 0; i < 6; i++) {
			stateVector[i] = State.$NullState$;
		}
		clearEvents();
		clearOutEvents();
	}
	
	public void enter() {
		if (!initialized) {
			throw new IllegalStateException(
					"The state machine needs to be initialized first by calling the init() function.");
		}
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		enterSequence_ModeSwitching_default();
		enterSequence_Light_default();
		enterSequence_Time_default();
		enterSequence_ChronoOnOff_default();
		enterSequence_Alarm_default();
	}
	
	public void exit() {
		exitSequence_ModeSwitching();
		exitSequence_Light();
		exitSequence_Time();
		exitSequence_ChronoOnOff();
		exitSequence_Alarm();
	}
	
	/**
	 * @see IStatemachine#isActive()
	 */
	public boolean isActive() {
		return stateVector[0] != State.$NullState$||stateVector[1] != State.$NullState$||stateVector[2] != State.$NullState$||stateVector[3] != State.$NullState$||stateVector[4] != State.$NullState$||stateVector[5] != State.$NullState$;
	}
	
	/** 
	* Always returns 'false' since this state machine can never become final.
	*
	* @see IStatemachine#isFinal()
	*/
	public boolean isFinal() {
		return false;
	}
	/**
	* This method resets the incoming events (time events included).
	*/
	protected void clearEvents() {
		sCIButtons.clearEvents();
		sCITimeEvents.clearEvents();
		sCILogicUnit.clearEvents();
		for (int i=0; i<timeEvents.length; i++) {
			timeEvents[i] = false;
		}
	}
	
	/**
	* This method resets the outgoing events.
	*/
	protected void clearOutEvents() {
	}
	
	/**
	* Returns true if the given state is currently active otherwise false.
	*/
	public boolean isStateActive(State state) {
	
		switch (state) {
		case modeSwitching_ChronoMode:
			return stateVector[0] == State.modeSwitching_ChronoMode;
		case modeSwitching_TimeMode:
			return stateVector[0] == State.modeSwitching_TimeMode;
		case modeSwitching_TimeEditingMode:
			return stateVector[0] == State.modeSwitching_TimeEditingMode;
		case modeSwitching_RightBottomButtonPressed:
			return stateVector[0] == State.modeSwitching_RightBottomButtonPressed;
		case modeSwitching_LeftBottomButtonPressed:
			return stateVector[0] == State.modeSwitching_LeftBottomButtonPressed;
		case modeSwitching_EditingMode:
			return stateVector[0].ordinal() >= State.
					modeSwitching_EditingMode.ordinal()&& stateVector[0].ordinal() <= State.modeSwitching_EditingMode_r2_SelectionOff.ordinal();
		case modeSwitching_EditingMode_r1_editCurrent:
			return stateVector[0] == State.modeSwitching_EditingMode_r1_editCurrent;
		case modeSwitching_EditingMode_r1_LeftBottomButtonPressed:
			return stateVector[0] == State.modeSwitching_EditingMode_r1_LeftBottomButtonPressed;
		case modeSwitching_EditingMode_r1_BottomRightPressed:
			return stateVector[0] == State.modeSwitching_EditingMode_r1_BottomRightPressed;
		case modeSwitching_EditingMode_r2_SelectionOn:
			return stateVector[1] == State.modeSwitching_EditingMode_r2_SelectionOn;
		case modeSwitching_EditingMode_r2_SelectionOff:
			return stateVector[1] == State.modeSwitching_EditingMode_r2_SelectionOff;
		case light_lightoff:
			return stateVector[2] == State.light_lightoff;
		case light_lighton:
			return stateVector[2] == State.light_lighton;
		case time_digitalwatch:
			return stateVector[3] == State.time_digitalwatch;
		case time_timePause:
			return stateVector[3] == State.time_timePause;
		case chronoOnOff_ChronoOff:
			return stateVector[4] == State.chronoOnOff_ChronoOff;
		case chronoOnOff_ChronoOn:
			return stateVector[4] == State.chronoOnOff_ChronoOn;
		case alarm_AlarmOff:
			return stateVector[5] == State.alarm_AlarmOff;
		case alarm_AlarmOn:
			return stateVector[5].ordinal() >= State.
					alarm_AlarmOn.ordinal()&& stateVector[5].ordinal() <= State.alarm_AlarmOn_r1_BlinkOff.ordinal();
		case alarm_AlarmOn_r1_BlinkOn:
			return stateVector[5] == State.alarm_AlarmOn_r1_BlinkOn;
		case alarm_AlarmOn_r1_BlinkOff:
			return stateVector[5] == State.alarm_AlarmOn_r1_BlinkOff;
		default:
			return false;
		}
	}
	
	/**
	* Set the {@link ITimer} for the state machine. It must be set
	* externally on a timed state machine before a run cycle can be correct
	* executed.
	* 
	* @param timer
	*/
	public void setTimer(ITimer timer) {
		this.timer = timer;
	}
	
	/**
	* Returns the currently used timer.
	* 
	* @return {@link ITimer}
	*/
	public ITimer getTimer() {
		return timer;
	}
	
	public void timeElapsed(int eventID) {
		timeEvents[eventID] = true;
	}
	
	public SCIButtons getSCIButtons() {
		return sCIButtons;
	}
	
	public SCITimeEvents getSCITimeEvents() {
		return sCITimeEvents;
	}
	
	public SCIDisplay getSCIDisplay() {
		return sCIDisplay;
	}
	
	public SCILogicUnit getSCILogicUnit() {
		return sCILogicUnit;
	}
	
	private boolean check_ModeSwitching_ChronoMode_tr0_tr0() {
		return (sCIButtons.topLeftPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_ChronoMode_tr1_tr1() {
		return (sCIButtons.bottomLeftPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_ChronoMode_tr2_tr2() {
		return timeEvents[0];
	}
	
	private boolean check_ModeSwitching_TimeMode_tr0_tr0() {
		return (sCIButtons.topLeftPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_TimeMode_tr1_tr1() {
		return (sCIButtons.bottomRightPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_TimeMode_tr2_tr2() {
		return (sCIButtons.bottomLeftPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_TimeEditingMode_tr0_tr0() {
		return true;
	}
	
	private boolean check_ModeSwitching_RightBottomButtonPressed_tr0_tr0() {
		return timeEvents[1];
	}
	
	private boolean check_ModeSwitching_RightBottomButtonPressed_tr1_tr1() {
		return sCIButtons.bottomRightReleased;
	}
	
	private boolean check_ModeSwitching_LeftBottomButtonPressed_tr0_tr0() {
		return sCIButtons.bottomLeftReleased;
	}
	
	private boolean check_ModeSwitching_LeftBottomButtonPressed_tr1_tr1() {
		return timeEvents[2];
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_editCurrent_tr0_tr0() {
		return (sCIButtons.bottomLeftPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_editCurrent_tr1_tr1() {
		return (sCIButtons.bottomRightPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_editCurrent_tr2_tr2() {
		return timeEvents[3];
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr0_tr0() {
		return timeEvents[4];
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr1_tr1() {
		return sCIButtons.bottomLeftReleased;
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_BottomRightPressed_tr0_tr0() {
		return timeEvents[5];
	}
	
	private boolean check_ModeSwitching_EditingMode_r1_BottomRightPressed_tr1_tr1() {
		return sCIButtons.bottomRightReleased;
	}
	
	private boolean check_ModeSwitching_EditingMode_r2_SelectionOn_tr0_tr0() {
		return timeEvents[6];
	}
	
	private boolean check_ModeSwitching_EditingMode_r2_SelectionOff_tr0_tr0() {
		return timeEvents[7];
	}
	
	private boolean check_Light_lightoff_tr0_tr0() {
		return (sCIButtons.topRightPressed) && (isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_Light_lightoff_lr0_lr0() {
		return timeEvents[8];
	}
	
	private boolean check_Light_lighton_tr0_tr0() {
		return sCIButtons.topRightReleased;
	}
	
	private boolean check_Time_digitalwatch_tr0_tr0() {
		return (timeEvents[9]) && ( !isStateActive(State.modeSwitching_TimeEditingMode));
	}
	
	private boolean check_Time_digitalwatch_tr1_tr1() {
		return sCITimeEvents.startTimeEditing;
	}
	
	private boolean check_Time_timePause_tr0_tr0() {
		return sCITimeEvents.stopTimeEditing;
	}
	
	private boolean check_ChronoOnOff_ChronoOff_tr0_tr0() {
		return (sCIButtons.bottomRightPressed) && (isStateActive(State.modeSwitching_ChronoMode) && isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ChronoOnOff_ChronoOn_tr0_tr0() {
		return (sCIButtons.bottomRightPressed) && (isStateActive(State.modeSwitching_ChronoMode) && isStateActive(State.alarm_AlarmOff));
	}
	
	private boolean check_ChronoOnOff_ChronoOn_lr0_lr0() {
		return timeEvents[10];
	}
	
	private boolean check_Alarm_AlarmOff_tr0_tr0() {
		return sCILogicUnit.startAlarm;
	}
	
	private boolean check_Alarm_AlarmOn_tr0_tr0() {
		return timeEvents[11];
	}
	
	private boolean check_Alarm_AlarmOn_tr1_tr1() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_Alarm_AlarmOn_tr2_tr2() {
		return sCIButtons.bottomLeftPressed;
	}
	
	private boolean check_Alarm_AlarmOn_tr4_tr4() {
		return sCIButtons.topLeftPressed;
	}
	
	private boolean check_Alarm_AlarmOn_r1_BlinkOn_tr0_tr0() {
		return timeEvents[12];
	}
	
	private boolean check_Alarm_AlarmOn_r1_BlinkOff_tr0_tr0() {
		return timeEvents[13];
	}
	
	private void effect_ModeSwitching_ChronoMode_tr0() {
		exitSequence_ModeSwitching_ChronoMode();
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	private void effect_ModeSwitching_ChronoMode_tr1() {
		exitSequence_ModeSwitching_ChronoMode();
		sCILogicUnit.operationCallback.resetChrono();
		
		enterSequence_ModeSwitching_ChronoMode_default();
	}
	
	private void effect_ModeSwitching_ChronoMode_tr2() {
		exitSequence_ModeSwitching_ChronoMode();
		sCIDisplay.operationCallback.refreshChronoDisplay();
		
		enterSequence_ModeSwitching_ChronoMode_default();
	}
	
	private void effect_ModeSwitching_TimeMode_tr0() {
		exitSequence_ModeSwitching_TimeMode();
		enterSequence_ModeSwitching_ChronoMode_default();
	}
	
	private void effect_ModeSwitching_TimeMode_tr1() {
		exitSequence_ModeSwitching_TimeMode();
		enterSequence_ModeSwitching_RightBottomButtonPressed_default();
	}
	
	private void effect_ModeSwitching_TimeMode_tr2() {
		exitSequence_ModeSwitching_TimeMode();
		sCILogicUnit.operationCallback.setAlarm();
		
		enterSequence_ModeSwitching_LeftBottomButtonPressed_default();
	}
	
	private void effect_ModeSwitching_TimeEditingMode_tr0() {
		exitSequence_ModeSwitching_TimeEditingMode();
		enterSequence_ModeSwitching_EditingMode_default();
	}
	
	private void effect_ModeSwitching_RightBottomButtonPressed_tr0() {
		exitSequence_ModeSwitching_RightBottomButtonPressed();
		sCILogicUnit.operationCallback.startTimeEditMode();
		
		enterSequence_ModeSwitching_TimeEditingMode_default();
	}
	
	private void effect_ModeSwitching_RightBottomButtonPressed_tr1() {
		exitSequence_ModeSwitching_RightBottomButtonPressed();
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	private void effect_ModeSwitching_LeftBottomButtonPressed_tr0() {
		exitSequence_ModeSwitching_LeftBottomButtonPressed();
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	private void effect_ModeSwitching_LeftBottomButtonPressed_tr1() {
		exitSequence_ModeSwitching_LeftBottomButtonPressed();
		sCILogicUnit.operationCallback.startAlarmEditMode();
		
		enterSequence_ModeSwitching_EditingMode_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_editCurrent_tr0() {
		exitSequence_ModeSwitching_EditingMode_r1_editCurrent();
		enterSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_editCurrent_tr1() {
		exitSequence_ModeSwitching_EditingMode_r1_editCurrent();
		enterSequence_ModeSwitching_EditingMode_r1_BottomRightPressed_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_editCurrent_tr2() {
		exitSequence_ModeSwitching_EditingMode();
		sCITimeEvents.raiseStopTimeEditing();
		
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr0() {
		exitSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
		sCILogicUnit.operationCallback.increaseSelection();
		
		enterSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr1() {
		exitSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
		sCILogicUnit.operationCallback.increaseSelection();
		
		enterSequence_ModeSwitching_EditingMode_r1_editCurrent_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_BottomRightPressed_tr0() {
		exitSequence_ModeSwitching_EditingMode();
		sCITimeEvents.raiseStopTimeEditing();
		
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r1_BottomRightPressed_tr1() {
		exitSequence_ModeSwitching_EditingMode_r1_BottomRightPressed();
		sCILogicUnit.operationCallback.selectNext();
		
		enterSequence_ModeSwitching_EditingMode_r1_editCurrent_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r2_SelectionOn_tr0() {
		exitSequence_ModeSwitching_EditingMode_r2_SelectionOn();
		enterSequence_ModeSwitching_EditingMode_r2_SelectionOff_default();
	}
	
	private void effect_ModeSwitching_EditingMode_r2_SelectionOff_tr0() {
		exitSequence_ModeSwitching_EditingMode_r2_SelectionOff();
		enterSequence_ModeSwitching_EditingMode_r2_SelectionOn_default();
	}
	
	private void effect_Light_lightoff_tr0() {
		exitSequence_Light_lightoff();
		enterSequence_Light_lighton_default();
	}
	
	private void effect_Light_lightoff_lr0_lr0() {
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	private void effect_Light_lighton_tr0() {
		exitSequence_Light_lighton();
		enterSequence_Light_lightoff_default();
	}
	
	private void effect_Time_digitalwatch_tr0() {
		exitSequence_Time_digitalwatch();
		sCILogicUnit.operationCallback.increaseTimeByOne();
		
		enterSequence_Time_digitalwatch_default();
	}
	
	private void effect_Time_digitalwatch_tr1() {
		exitSequence_Time_digitalwatch();
		enterSequence_Time_timePause_default();
	}
	
	private void effect_Time_timePause_tr0() {
		exitSequence_Time_timePause();
		enterSequence_Time_digitalwatch_default();
	}
	
	private void effect_ChronoOnOff_ChronoOff_tr0() {
		exitSequence_ChronoOnOff_ChronoOff();
		enterSequence_ChronoOnOff_ChronoOn_default();
	}
	
	private void effect_ChronoOnOff_ChronoOn_tr0() {
		exitSequence_ChronoOnOff_ChronoOn();
		enterSequence_ChronoOnOff_ChronoOff_default();
	}
	
	private void effect_ChronoOnOff_ChronoOn_lr0_lr0() {
		sCILogicUnit.operationCallback.increaseChronoByOne();
	}
	
	private void effect_Alarm_AlarmOff_tr0() {
		exitSequence_Alarm_AlarmOff();
		enterSequence_Alarm_AlarmOn_default();
	}
	
	private void effect_Alarm_AlarmOn_tr0() {
		exitSequence_Alarm_AlarmOn();
		enterSequence_Alarm_AlarmOff_default();
	}
	
	private void effect_Alarm_AlarmOn_tr1() {
		exitSequence_Alarm_AlarmOn();
		enterSequence_Alarm_AlarmOff_default();
	}
	
	private void effect_Alarm_AlarmOn_tr2() {
		exitSequence_Alarm_AlarmOn();
		enterSequence_Alarm_AlarmOff_default();
	}
	
	private void effect_Alarm_AlarmOn_tr4() {
		exitSequence_Alarm_AlarmOn();
		enterSequence_Alarm_AlarmOff_default();
	}
	
	private void effect_Alarm_AlarmOn_r1_BlinkOn_tr0() {
		exitSequence_Alarm_AlarmOn_r1_BlinkOn();
		enterSequence_Alarm_AlarmOn_r1_BlinkOff_default();
	}
	
	private void effect_Alarm_AlarmOn_r1_BlinkOff_tr0() {
		exitSequence_Alarm_AlarmOn_r1_BlinkOff();
		enterSequence_Alarm_AlarmOn_r1_BlinkOn_default();
	}
	
	/* Entry action for state 'ChronoMode'. */
	private void entryAction_ModeSwitching_ChronoMode() {
		timer.setTimer(this, 0, 250, false);
		
		sCIDisplay.operationCallback.refreshChronoDisplay();
	}
	
	/* Entry action for state 'TimeEditingMode'. */
	private void entryAction_ModeSwitching_TimeEditingMode() {
		sCITimeEvents.raiseStartTimeEditing();
	}
	
	/* Entry action for state 'RightBottomButtonPressed'. */
	private void entryAction_ModeSwitching_RightBottomButtonPressed() {
		timer.setTimer(this, 1, 1500, false);
	}
	
	/* Entry action for state 'LeftBottomButtonPressed'. */
	private void entryAction_ModeSwitching_LeftBottomButtonPressed() {
		timer.setTimer(this, 2, 1500, false);
	}
	
	/* Entry action for state 'editCurrent'. */
	private void entryAction_ModeSwitching_EditingMode_r1_editCurrent() {
		timer.setTimer(this, 3, 5*1000, false);
	}
	
	/* Entry action for state 'LeftBottomButtonPressed'. */
	private void entryAction_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed() {
		timer.setTimer(this, 4, 300, false);
	}
	
	/* Entry action for state 'BottomRightPressed'. */
	private void entryAction_ModeSwitching_EditingMode_r1_BottomRightPressed() {
		timer.setTimer(this, 5, 2*1000, false);
	}
	
	/* Entry action for state 'SelectionOn'. */
	private void entryAction_ModeSwitching_EditingMode_r2_SelectionOn() {
		timer.setTimer(this, 6, 250, false);
		
		sCIDisplay.operationCallback.showSelection();
	}
	
	/* Entry action for state 'SelectionOff'. */
	private void entryAction_ModeSwitching_EditingMode_r2_SelectionOff() {
		timer.setTimer(this, 7, 250, false);
		
		sCIDisplay.operationCallback.hideSelection();
	}
	
	/* Entry action for state 'lightoff'. */
	private void entryAction_Light_lightoff() {
		timer.setTimer(this, 8, 2*1000, false);
	}
	
	/* Entry action for state 'lighton'. */
	private void entryAction_Light_lighton() {
		sCIDisplay.operationCallback.setIndiglo();
	}
	
	/* Entry action for state 'digitalwatch'. */
	private void entryAction_Time_digitalwatch() {
		timer.setTimer(this, 9, 1*1000, false);
		
		if (isStateActive(State.modeSwitching_TimeMode)) {
			sCIDisplay.operationCallback.refreshDateDisplay();
		}
		if (isStateActive(State.modeSwitching_TimeMode)) {
			sCIDisplay.operationCallback.refreshTimeDisplay();
		}
		if (isStateActive(State.modeSwitching_TimeMode) || isStateActive(State.modeSwitching_EditingMode)) {
			sCIDisplay.operationCallback.refreshAlarmDisplay();
		}
	}
	
	/* Entry action for state 'ChronoOn'. */
	private void entryAction_ChronoOnOff_ChronoOn() {
		timer.setTimer(this, 10, 250, true);
	}
	
	/* Entry action for state 'AlarmOn'. */
	private void entryAction_Alarm_AlarmOn() {
		timer.setTimer(this, 11, 4*1000, false);
	}
	
	/* Entry action for state 'BlinkOn'. */
	private void entryAction_Alarm_AlarmOn_r1_BlinkOn() {
		timer.setTimer(this, 12, 500, false);
		
		sCIDisplay.operationCallback.setIndiglo();
	}
	
	/* Entry action for state 'BlinkOff'. */
	private void entryAction_Alarm_AlarmOn_r1_BlinkOff() {
		timer.setTimer(this, 13, 500, false);
		
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	/* Exit action for state 'ChronoMode'. */
	private void exitAction_ModeSwitching_ChronoMode() {
		timer.unsetTimer(this, 0);
	}
	
	/* Exit action for state 'RightBottomButtonPressed'. */
	private void exitAction_ModeSwitching_RightBottomButtonPressed() {
		timer.unsetTimer(this, 1);
	}
	
	/* Exit action for state 'LeftBottomButtonPressed'. */
	private void exitAction_ModeSwitching_LeftBottomButtonPressed() {
		timer.unsetTimer(this, 2);
	}
	
	/* Exit action for state 'editCurrent'. */
	private void exitAction_ModeSwitching_EditingMode_r1_editCurrent() {
		timer.unsetTimer(this, 3);
	}
	
	/* Exit action for state 'LeftBottomButtonPressed'. */
	private void exitAction_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed() {
		timer.unsetTimer(this, 4);
	}
	
	/* Exit action for state 'BottomRightPressed'. */
	private void exitAction_ModeSwitching_EditingMode_r1_BottomRightPressed() {
		timer.unsetTimer(this, 5);
	}
	
	/* Exit action for state 'SelectionOn'. */
	private void exitAction_ModeSwitching_EditingMode_r2_SelectionOn() {
		timer.unsetTimer(this, 6);
	}
	
	/* Exit action for state 'SelectionOff'. */
	private void exitAction_ModeSwitching_EditingMode_r2_SelectionOff() {
		timer.unsetTimer(this, 7);
	}
	
	/* Exit action for state 'lightoff'. */
	private void exitAction_Light_lightoff() {
		timer.unsetTimer(this, 8);
	}
	
	/* Exit action for state 'digitalwatch'. */
	private void exitAction_Time_digitalwatch() {
		timer.unsetTimer(this, 9);
	}
	
	/* Exit action for state 'ChronoOn'. */
	private void exitAction_ChronoOnOff_ChronoOn() {
		timer.unsetTimer(this, 10);
	}
	
	/* Exit action for state 'AlarmOn'. */
	private void exitAction_Alarm_AlarmOn() {
		timer.unsetTimer(this, 11);
		
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	/* Exit action for state 'BlinkOn'. */
	private void exitAction_Alarm_AlarmOn_r1_BlinkOn() {
		timer.unsetTimer(this, 12);
	}
	
	/* Exit action for state 'BlinkOff'. */
	private void exitAction_Alarm_AlarmOn_r1_BlinkOff() {
		timer.unsetTimer(this, 13);
	}
	
	/* 'default' enter sequence for state ChronoMode */
	private void enterSequence_ModeSwitching_ChronoMode_default() {
		entryAction_ModeSwitching_ChronoMode();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_ChronoMode;
	}
	
	/* 'default' enter sequence for state TimeMode */
	private void enterSequence_ModeSwitching_TimeMode_default() {
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_TimeMode;
	}
	
	/* 'default' enter sequence for state TimeEditingMode */
	private void enterSequence_ModeSwitching_TimeEditingMode_default() {
		entryAction_ModeSwitching_TimeEditingMode();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_TimeEditingMode;
	}
	
	/* 'default' enter sequence for state RightBottomButtonPressed */
	private void enterSequence_ModeSwitching_RightBottomButtonPressed_default() {
		entryAction_ModeSwitching_RightBottomButtonPressed();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_RightBottomButtonPressed;
	}
	
	/* 'default' enter sequence for state LeftBottomButtonPressed */
	private void enterSequence_ModeSwitching_LeftBottomButtonPressed_default() {
		entryAction_ModeSwitching_LeftBottomButtonPressed();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_LeftBottomButtonPressed;
	}
	
	/* 'default' enter sequence for state EditingMode */
	private void enterSequence_ModeSwitching_EditingMode_default() {
		enterSequence_ModeSwitching_EditingMode_r1_default();
		enterSequence_ModeSwitching_EditingMode_r2_default();
	}
	
	/* 'default' enter sequence for state editCurrent */
	private void enterSequence_ModeSwitching_EditingMode_r1_editCurrent_default() {
		entryAction_ModeSwitching_EditingMode_r1_editCurrent();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_EditingMode_r1_editCurrent;
	}
	
	/* 'default' enter sequence for state LeftBottomButtonPressed */
	private void enterSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_default() {
		entryAction_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_EditingMode_r1_LeftBottomButtonPressed;
	}
	
	/* 'default' enter sequence for state BottomRightPressed */
	private void enterSequence_ModeSwitching_EditingMode_r1_BottomRightPressed_default() {
		entryAction_ModeSwitching_EditingMode_r1_BottomRightPressed();
		nextStateIndex = 0;
		stateVector[0] = State.modeSwitching_EditingMode_r1_BottomRightPressed;
	}
	
	/* 'default' enter sequence for state SelectionOn */
	private void enterSequence_ModeSwitching_EditingMode_r2_SelectionOn_default() {
		entryAction_ModeSwitching_EditingMode_r2_SelectionOn();
		nextStateIndex = 1;
		stateVector[1] = State.modeSwitching_EditingMode_r2_SelectionOn;
	}
	
	/* 'default' enter sequence for state SelectionOff */
	private void enterSequence_ModeSwitching_EditingMode_r2_SelectionOff_default() {
		entryAction_ModeSwitching_EditingMode_r2_SelectionOff();
		nextStateIndex = 1;
		stateVector[1] = State.modeSwitching_EditingMode_r2_SelectionOff;
	}
	
	/* 'default' enter sequence for state lightoff */
	private void enterSequence_Light_lightoff_default() {
		entryAction_Light_lightoff();
		nextStateIndex = 2;
		stateVector[2] = State.light_lightoff;
	}
	
	/* 'default' enter sequence for state lighton */
	private void enterSequence_Light_lighton_default() {
		entryAction_Light_lighton();
		nextStateIndex = 2;
		stateVector[2] = State.light_lighton;
	}
	
	/* 'default' enter sequence for state digitalwatch */
	private void enterSequence_Time_digitalwatch_default() {
		entryAction_Time_digitalwatch();
		nextStateIndex = 3;
		stateVector[3] = State.time_digitalwatch;
	}
	
	/* 'default' enter sequence for state timePause */
	private void enterSequence_Time_timePause_default() {
		nextStateIndex = 3;
		stateVector[3] = State.time_timePause;
	}
	
	/* 'default' enter sequence for state ChronoOff */
	private void enterSequence_ChronoOnOff_ChronoOff_default() {
		nextStateIndex = 4;
		stateVector[4] = State.chronoOnOff_ChronoOff;
	}
	
	/* 'default' enter sequence for state ChronoOn */
	private void enterSequence_ChronoOnOff_ChronoOn_default() {
		entryAction_ChronoOnOff_ChronoOn();
		nextStateIndex = 4;
		stateVector[4] = State.chronoOnOff_ChronoOn;
	}
	
	/* 'default' enter sequence for state AlarmOff */
	private void enterSequence_Alarm_AlarmOff_default() {
		nextStateIndex = 5;
		stateVector[5] = State.alarm_AlarmOff;
	}
	
	/* 'default' enter sequence for state AlarmOn */
	private void enterSequence_Alarm_AlarmOn_default() {
		entryAction_Alarm_AlarmOn();
		enterSequence_Alarm_AlarmOn_r1_default();
	}
	
	/* 'default' enter sequence for state BlinkOn */
	private void enterSequence_Alarm_AlarmOn_r1_BlinkOn_default() {
		entryAction_Alarm_AlarmOn_r1_BlinkOn();
		nextStateIndex = 5;
		stateVector[5] = State.alarm_AlarmOn_r1_BlinkOn;
	}
	
	/* 'default' enter sequence for state BlinkOff */
	private void enterSequence_Alarm_AlarmOn_r1_BlinkOff_default() {
		entryAction_Alarm_AlarmOn_r1_BlinkOff();
		nextStateIndex = 5;
		stateVector[5] = State.alarm_AlarmOn_r1_BlinkOff;
	}
	
	/* 'default' enter sequence for region ModeSwitching */
	private void enterSequence_ModeSwitching_default() {
		react_ModeSwitching__entry_Default();
	}
	
	/* 'default' enter sequence for region r1 */
	private void enterSequence_ModeSwitching_EditingMode_r1_default() {
		react_ModeSwitching_EditingMode_r1__entry_Default();
	}
	
	/* 'default' enter sequence for region r2 */
	private void enterSequence_ModeSwitching_EditingMode_r2_default() {
		react_ModeSwitching_EditingMode_r2__entry_Default();
	}
	
	/* 'default' enter sequence for region Light */
	private void enterSequence_Light_default() {
		react_Light__entry_Default();
	}
	
	/* 'default' enter sequence for region Time */
	private void enterSequence_Time_default() {
		react_Time__entry_Default();
	}
	
	/* 'default' enter sequence for region ChronoOnOff */
	private void enterSequence_ChronoOnOff_default() {
		react_ChronoOnOff__entry_Default();
	}
	
	/* 'default' enter sequence for region Alarm */
	private void enterSequence_Alarm_default() {
		react_Alarm__entry_Default();
	}
	
	/* 'default' enter sequence for region r1 */
	private void enterSequence_Alarm_AlarmOn_r1_default() {
		react_Alarm_AlarmOn_r1__entry_Default();
	}
	
	/* Default exit sequence for state ChronoMode */
	private void exitSequence_ModeSwitching_ChronoMode() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_ChronoMode();
	}
	
	/* Default exit sequence for state TimeMode */
	private void exitSequence_ModeSwitching_TimeMode() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
	}
	
	/* Default exit sequence for state TimeEditingMode */
	private void exitSequence_ModeSwitching_TimeEditingMode() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
	}
	
	/* Default exit sequence for state RightBottomButtonPressed */
	private void exitSequence_ModeSwitching_RightBottomButtonPressed() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_RightBottomButtonPressed();
	}
	
	/* Default exit sequence for state LeftBottomButtonPressed */
	private void exitSequence_ModeSwitching_LeftBottomButtonPressed() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_LeftBottomButtonPressed();
	}
	
	/* Default exit sequence for state EditingMode */
	private void exitSequence_ModeSwitching_EditingMode() {
		exitSequence_ModeSwitching_EditingMode_r1();
		exitSequence_ModeSwitching_EditingMode_r2();
	}
	
	/* Default exit sequence for state editCurrent */
	private void exitSequence_ModeSwitching_EditingMode_r1_editCurrent() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_EditingMode_r1_editCurrent();
	}
	
	/* Default exit sequence for state LeftBottomButtonPressed */
	private void exitSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
	}
	
	/* Default exit sequence for state BottomRightPressed */
	private void exitSequence_ModeSwitching_EditingMode_r1_BottomRightPressed() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_ModeSwitching_EditingMode_r1_BottomRightPressed();
	}
	
	/* Default exit sequence for state SelectionOn */
	private void exitSequence_ModeSwitching_EditingMode_r2_SelectionOn() {
		nextStateIndex = 1;
		stateVector[1] = State.$NullState$;
		
		exitAction_ModeSwitching_EditingMode_r2_SelectionOn();
	}
	
	/* Default exit sequence for state SelectionOff */
	private void exitSequence_ModeSwitching_EditingMode_r2_SelectionOff() {
		nextStateIndex = 1;
		stateVector[1] = State.$NullState$;
		
		exitAction_ModeSwitching_EditingMode_r2_SelectionOff();
	}
	
	/* Default exit sequence for state lightoff */
	private void exitSequence_Light_lightoff() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_Light_lightoff();
	}
	
	/* Default exit sequence for state lighton */
	private void exitSequence_Light_lighton() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state digitalwatch */
	private void exitSequence_Time_digitalwatch() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
		
		exitAction_Time_digitalwatch();
	}
	
	/* Default exit sequence for state timePause */
	private void exitSequence_Time_timePause() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
	}
	
	/* Default exit sequence for state ChronoOff */
	private void exitSequence_ChronoOnOff_ChronoOff() {
		nextStateIndex = 4;
		stateVector[4] = State.$NullState$;
	}
	
	/* Default exit sequence for state ChronoOn */
	private void exitSequence_ChronoOnOff_ChronoOn() {
		nextStateIndex = 4;
		stateVector[4] = State.$NullState$;
		
		exitAction_ChronoOnOff_ChronoOn();
	}
	
	/* Default exit sequence for state AlarmOff */
	private void exitSequence_Alarm_AlarmOff() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
	}
	
	/* Default exit sequence for state AlarmOn */
	private void exitSequence_Alarm_AlarmOn() {
		exitSequence_Alarm_AlarmOn_r1();
		exitAction_Alarm_AlarmOn();
	}
	
	/* Default exit sequence for state BlinkOn */
	private void exitSequence_Alarm_AlarmOn_r1_BlinkOn() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
		
		exitAction_Alarm_AlarmOn_r1_BlinkOn();
	}
	
	/* Default exit sequence for state BlinkOff */
	private void exitSequence_Alarm_AlarmOn_r1_BlinkOff() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
		
		exitAction_Alarm_AlarmOn_r1_BlinkOff();
	}
	
	/* Default exit sequence for region ModeSwitching */
	private void exitSequence_ModeSwitching() {
		switch (stateVector[0]) {
		case modeSwitching_ChronoMode:
			exitSequence_ModeSwitching_ChronoMode();
			break;
		case modeSwitching_TimeMode:
			exitSequence_ModeSwitching_TimeMode();
			break;
		case modeSwitching_TimeEditingMode:
			exitSequence_ModeSwitching_TimeEditingMode();
			break;
		case modeSwitching_RightBottomButtonPressed:
			exitSequence_ModeSwitching_RightBottomButtonPressed();
			break;
		case modeSwitching_LeftBottomButtonPressed:
			exitSequence_ModeSwitching_LeftBottomButtonPressed();
			break;
		case modeSwitching_EditingMode_r1_editCurrent:
			exitSequence_ModeSwitching_EditingMode_r1_editCurrent();
			break;
		case modeSwitching_EditingMode_r1_LeftBottomButtonPressed:
			exitSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
			break;
		case modeSwitching_EditingMode_r1_BottomRightPressed:
			exitSequence_ModeSwitching_EditingMode_r1_BottomRightPressed();
			break;
		default:
			break;
		}
		
		switch (stateVector[1]) {
		case modeSwitching_EditingMode_r2_SelectionOn:
			exitSequence_ModeSwitching_EditingMode_r2_SelectionOn();
			break;
		case modeSwitching_EditingMode_r2_SelectionOff:
			exitSequence_ModeSwitching_EditingMode_r2_SelectionOff();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region r1 */
	private void exitSequence_ModeSwitching_EditingMode_r1() {
		switch (stateVector[0]) {
		case modeSwitching_EditingMode_r1_editCurrent:
			exitSequence_ModeSwitching_EditingMode_r1_editCurrent();
			break;
		case modeSwitching_EditingMode_r1_LeftBottomButtonPressed:
			exitSequence_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
			break;
		case modeSwitching_EditingMode_r1_BottomRightPressed:
			exitSequence_ModeSwitching_EditingMode_r1_BottomRightPressed();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region r2 */
	private void exitSequence_ModeSwitching_EditingMode_r2() {
		switch (stateVector[1]) {
		case modeSwitching_EditingMode_r2_SelectionOn:
			exitSequence_ModeSwitching_EditingMode_r2_SelectionOn();
			break;
		case modeSwitching_EditingMode_r2_SelectionOff:
			exitSequence_ModeSwitching_EditingMode_r2_SelectionOff();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region Light */
	private void exitSequence_Light() {
		switch (stateVector[2]) {
		case light_lightoff:
			exitSequence_Light_lightoff();
			break;
		case light_lighton:
			exitSequence_Light_lighton();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region Time */
	private void exitSequence_Time() {
		switch (stateVector[3]) {
		case time_digitalwatch:
			exitSequence_Time_digitalwatch();
			break;
		case time_timePause:
			exitSequence_Time_timePause();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region ChronoOnOff */
	private void exitSequence_ChronoOnOff() {
		switch (stateVector[4]) {
		case chronoOnOff_ChronoOff:
			exitSequence_ChronoOnOff_ChronoOff();
			break;
		case chronoOnOff_ChronoOn:
			exitSequence_ChronoOnOff_ChronoOn();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region Alarm */
	private void exitSequence_Alarm() {
		switch (stateVector[5]) {
		case alarm_AlarmOff:
			exitSequence_Alarm_AlarmOff();
			break;
		case alarm_AlarmOn_r1_BlinkOn:
			exitSequence_Alarm_AlarmOn_r1_BlinkOn();
			exitAction_Alarm_AlarmOn();
			break;
		case alarm_AlarmOn_r1_BlinkOff:
			exitSequence_Alarm_AlarmOn_r1_BlinkOff();
			exitAction_Alarm_AlarmOn();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region r1 */
	private void exitSequence_Alarm_AlarmOn_r1() {
		switch (stateVector[5]) {
		case alarm_AlarmOn_r1_BlinkOn:
			exitSequence_Alarm_AlarmOn_r1_BlinkOn();
			break;
		case alarm_AlarmOn_r1_BlinkOff:
			exitSequence_Alarm_AlarmOn_r1_BlinkOff();
			break;
		default:
			break;
		}
	}
	
	/* The reactions of state ChronoMode. */
	private void react_ModeSwitching_ChronoMode() {
		if (check_ModeSwitching_ChronoMode_tr0_tr0()) {
			effect_ModeSwitching_ChronoMode_tr0();
		} else {
			if (check_ModeSwitching_ChronoMode_tr1_tr1()) {
				effect_ModeSwitching_ChronoMode_tr1();
			} else {
				if (check_ModeSwitching_ChronoMode_tr2_tr2()) {
					effect_ModeSwitching_ChronoMode_tr2();
				}
			}
		}
	}
	
	/* The reactions of state TimeMode. */
	private void react_ModeSwitching_TimeMode() {
		if (check_ModeSwitching_TimeMode_tr0_tr0()) {
			effect_ModeSwitching_TimeMode_tr0();
		} else {
			if (check_ModeSwitching_TimeMode_tr1_tr1()) {
				effect_ModeSwitching_TimeMode_tr1();
			} else {
				if (check_ModeSwitching_TimeMode_tr2_tr2()) {
					effect_ModeSwitching_TimeMode_tr2();
				}
			}
		}
	}
	
	/* The reactions of state TimeEditingMode. */
	private void react_ModeSwitching_TimeEditingMode() {
		effect_ModeSwitching_TimeEditingMode_tr0();
	}
	
	/* The reactions of state RightBottomButtonPressed. */
	private void react_ModeSwitching_RightBottomButtonPressed() {
		if (check_ModeSwitching_RightBottomButtonPressed_tr0_tr0()) {
			effect_ModeSwitching_RightBottomButtonPressed_tr0();
		} else {
			if (check_ModeSwitching_RightBottomButtonPressed_tr1_tr1()) {
				effect_ModeSwitching_RightBottomButtonPressed_tr1();
			}
		}
	}
	
	/* The reactions of state LeftBottomButtonPressed. */
	private void react_ModeSwitching_LeftBottomButtonPressed() {
		if (check_ModeSwitching_LeftBottomButtonPressed_tr0_tr0()) {
			effect_ModeSwitching_LeftBottomButtonPressed_tr0();
		} else {
			if (check_ModeSwitching_LeftBottomButtonPressed_tr1_tr1()) {
				effect_ModeSwitching_LeftBottomButtonPressed_tr1();
			}
		}
	}
	
	/* The reactions of state editCurrent. */
	private void react_ModeSwitching_EditingMode_r1_editCurrent() {
		if (check_ModeSwitching_EditingMode_r1_editCurrent_tr0_tr0()) {
			effect_ModeSwitching_EditingMode_r1_editCurrent_tr0();
		} else {
			if (check_ModeSwitching_EditingMode_r1_editCurrent_tr1_tr1()) {
				effect_ModeSwitching_EditingMode_r1_editCurrent_tr1();
			} else {
				if (check_ModeSwitching_EditingMode_r1_editCurrent_tr2_tr2()) {
					effect_ModeSwitching_EditingMode_r1_editCurrent_tr2();
				}
			}
		}
	}
	
	/* The reactions of state LeftBottomButtonPressed. */
	private void react_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed() {
		if (check_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr0_tr0()) {
			effect_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr0();
		} else {
			if (check_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr1_tr1()) {
				effect_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed_tr1();
			}
		}
	}
	
	/* The reactions of state BottomRightPressed. */
	private void react_ModeSwitching_EditingMode_r1_BottomRightPressed() {
		if (check_ModeSwitching_EditingMode_r1_BottomRightPressed_tr0_tr0()) {
			effect_ModeSwitching_EditingMode_r1_BottomRightPressed_tr0();
		} else {
			if (check_ModeSwitching_EditingMode_r1_BottomRightPressed_tr1_tr1()) {
				effect_ModeSwitching_EditingMode_r1_BottomRightPressed_tr1();
			}
		}
	}
	
	/* The reactions of state SelectionOn. */
	private void react_ModeSwitching_EditingMode_r2_SelectionOn() {
		if (check_ModeSwitching_EditingMode_r2_SelectionOn_tr0_tr0()) {
			effect_ModeSwitching_EditingMode_r2_SelectionOn_tr0();
		}
	}
	
	/* The reactions of state SelectionOff. */
	private void react_ModeSwitching_EditingMode_r2_SelectionOff() {
		if (check_ModeSwitching_EditingMode_r2_SelectionOff_tr0_tr0()) {
			effect_ModeSwitching_EditingMode_r2_SelectionOff_tr0();
		}
	}
	
	/* The reactions of state lightoff. */
	private void react_Light_lightoff() {
		if (check_Light_lightoff_tr0_tr0()) {
			effect_Light_lightoff_tr0();
		} else {
			if (check_Light_lightoff_lr0_lr0()) {
				effect_Light_lightoff_lr0_lr0();
			}
		}
	}
	
	/* The reactions of state lighton. */
	private void react_Light_lighton() {
		if (check_Light_lighton_tr0_tr0()) {
			effect_Light_lighton_tr0();
		}
	}
	
	/* The reactions of state digitalwatch. */
	private void react_Time_digitalwatch() {
		if (check_Time_digitalwatch_tr0_tr0()) {
			effect_Time_digitalwatch_tr0();
		} else {
			if (check_Time_digitalwatch_tr1_tr1()) {
				effect_Time_digitalwatch_tr1();
			}
		}
	}
	
	/* The reactions of state timePause. */
	private void react_Time_timePause() {
		if (check_Time_timePause_tr0_tr0()) {
			effect_Time_timePause_tr0();
		}
	}
	
	/* The reactions of state ChronoOff. */
	private void react_ChronoOnOff_ChronoOff() {
		if (check_ChronoOnOff_ChronoOff_tr0_tr0()) {
			effect_ChronoOnOff_ChronoOff_tr0();
		}
	}
	
	/* The reactions of state ChronoOn. */
	private void react_ChronoOnOff_ChronoOn() {
		if (check_ChronoOnOff_ChronoOn_tr0_tr0()) {
			effect_ChronoOnOff_ChronoOn_tr0();
		} else {
			if (check_ChronoOnOff_ChronoOn_lr0_lr0()) {
				effect_ChronoOnOff_ChronoOn_lr0_lr0();
			}
		}
	}
	
	/* The reactions of state AlarmOff. */
	private void react_Alarm_AlarmOff() {
		if (check_Alarm_AlarmOff_tr0_tr0()) {
			effect_Alarm_AlarmOff_tr0();
		}
	}
	
	/* The reactions of state BlinkOn. */
	private void react_Alarm_AlarmOn_r1_BlinkOn() {
		if (check_Alarm_AlarmOn_tr0_tr0()) {
			effect_Alarm_AlarmOn_tr0();
		} else {
			if (check_Alarm_AlarmOn_tr1_tr1()) {
				effect_Alarm_AlarmOn_tr1();
			} else {
				if (check_Alarm_AlarmOn_tr2_tr2()) {
					effect_Alarm_AlarmOn_tr2();
				} else {
					if (check_Alarm_AlarmOn_tr4_tr4()) {
						effect_Alarm_AlarmOn_tr4();
					} else {
						if (check_Alarm_AlarmOn_r1_BlinkOn_tr0_tr0()) {
							effect_Alarm_AlarmOn_r1_BlinkOn_tr0();
						}
					}
				}
			}
		}
	}
	
	/* The reactions of state BlinkOff. */
	private void react_Alarm_AlarmOn_r1_BlinkOff() {
		if (check_Alarm_AlarmOn_tr0_tr0()) {
			effect_Alarm_AlarmOn_tr0();
		} else {
			if (check_Alarm_AlarmOn_tr1_tr1()) {
				effect_Alarm_AlarmOn_tr1();
			} else {
				if (check_Alarm_AlarmOn_tr2_tr2()) {
					effect_Alarm_AlarmOn_tr2();
				} else {
					if (check_Alarm_AlarmOn_tr4_tr4()) {
						effect_Alarm_AlarmOn_tr4();
					} else {
						if (check_Alarm_AlarmOn_r1_BlinkOff_tr0_tr0()) {
							effect_Alarm_AlarmOn_r1_BlinkOff_tr0();
						}
					}
				}
			}
		}
	}
	
	/* Default react sequence for initial entry  */
	private void react_ModeSwitching__entry_Default() {
		enterSequence_ModeSwitching_TimeMode_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_ModeSwitching_EditingMode_r1__entry_Default() {
		enterSequence_ModeSwitching_EditingMode_r1_editCurrent_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_ModeSwitching_EditingMode_r2__entry_Default() {
		enterSequence_ModeSwitching_EditingMode_r2_SelectionOn_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_Light__entry_Default() {
		enterSequence_Light_lightoff_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_Time__entry_Default() {
		enterSequence_Time_digitalwatch_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_ChronoOnOff__entry_Default() {
		enterSequence_ChronoOnOff_ChronoOff_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_Alarm__entry_Default() {
		enterSequence_Alarm_AlarmOff_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_Alarm_AlarmOn_r1__entry_Default() {
		enterSequence_Alarm_AlarmOn_r1_BlinkOn_default();
	}
	
	public void runCycle() {
		if (!initialized)
			throw new IllegalStateException(
					"The state machine needs to be initialized first by calling the init() function.");
		clearOutEvents();
		for (nextStateIndex = 0; nextStateIndex < stateVector.length; nextStateIndex++) {
			switch (stateVector[nextStateIndex]) {
			case modeSwitching_ChronoMode:
				react_ModeSwitching_ChronoMode();
				break;
			case modeSwitching_TimeMode:
				react_ModeSwitching_TimeMode();
				break;
			case modeSwitching_TimeEditingMode:
				react_ModeSwitching_TimeEditingMode();
				break;
			case modeSwitching_RightBottomButtonPressed:
				react_ModeSwitching_RightBottomButtonPressed();
				break;
			case modeSwitching_LeftBottomButtonPressed:
				react_ModeSwitching_LeftBottomButtonPressed();
				break;
			case modeSwitching_EditingMode_r1_editCurrent:
				react_ModeSwitching_EditingMode_r1_editCurrent();
				break;
			case modeSwitching_EditingMode_r1_LeftBottomButtonPressed:
				react_ModeSwitching_EditingMode_r1_LeftBottomButtonPressed();
				break;
			case modeSwitching_EditingMode_r1_BottomRightPressed:
				react_ModeSwitching_EditingMode_r1_BottomRightPressed();
				break;
			case modeSwitching_EditingMode_r2_SelectionOn:
				react_ModeSwitching_EditingMode_r2_SelectionOn();
				break;
			case modeSwitching_EditingMode_r2_SelectionOff:
				react_ModeSwitching_EditingMode_r2_SelectionOff();
				break;
			case light_lightoff:
				react_Light_lightoff();
				break;
			case light_lighton:
				react_Light_lighton();
				break;
			case time_digitalwatch:
				react_Time_digitalwatch();
				break;
			case time_timePause:
				react_Time_timePause();
				break;
			case chronoOnOff_ChronoOff:
				react_ChronoOnOff_ChronoOff();
				break;
			case chronoOnOff_ChronoOn:
				react_ChronoOnOff_ChronoOn();
				break;
			case alarm_AlarmOff:
				react_Alarm_AlarmOff();
				break;
			case alarm_AlarmOn_r1_BlinkOn:
				react_Alarm_AlarmOn_r1_BlinkOn();
				break;
			case alarm_AlarmOn_r1_BlinkOff:
				react_Alarm_AlarmOn_r1_BlinkOff();
				break;
			default:
				// $NullState$
			}
		}
		clearEvents();
	}
}
